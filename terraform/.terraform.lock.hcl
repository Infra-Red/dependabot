# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/digitalocean/digitalocean" {
  version     = "2.12.1"
  constraints = "~> 2.0"
  hashes = [
    "h1:T8E8vhtOFfftyuDPOm8f0wfzzoMxSNmGol8uz5riUpc=",
    "zh:097b5a5a355931b3e8cb446a303d48c92effe4a5c7105e7a258b2285bb814f05",
    "zh:58b5aa9e7f5c2904a017c4c8e92e9ba848e9ed1b0c01bf394d194948049411e1",
    "zh:7c692b6a383f106966651c2ed0a1696e3405429ff9323e8af7d19a807ff5a957",
    "zh:7ce04e593d31ae4e6d8b8b26cdd3e46c422057c1d406658c79fb832589b1ef42",
    "zh:8680c6b2525e6c8c7a699e61ade6635e1de8f4536228a91b652aae0d1d5e359e",
    "zh:8c2d56ae529ae7e2a8ae28146dd6ad3060f7ba2c708b8a389da0f61c162d95ec",
    "zh:93bc52dfc3aa178c497e73bbe9ea35b9b0ed47c0a08d1c65c8170e93b792aaaa",
    "zh:ad76cdfc5948eb15e0161afeec9d0ada1bbf0a02c6d0a96d75d96f0fd339a3ed",
    "zh:b0547f22ef66ad3a8b3c9eba532194de77e10505193012e8fba3ebbe7fa4eb97",
    "zh:bbd29b5f4436235d6f1c374fdcb3641c4b9ddb85de47d5a0bb0fdaac3fb0961d",
    "zh:bff558fa3e74af245d5e633cabf718210a708256fdc1d4abfbb71a29ce9301b2",
    "zh:cad01c97427a54c34c57192605290911c83d69ec76cbb327d47e5e50765a27ce",
    "zh:cd8af94e773eb185b579220c2884cce4456fd8f39c8f24d6fcc68b91093f1a87",
    "zh:cf2b94be1ac5b435e1f5ebaa04c9e919ffde3fdf80eefb0211fad005e4a26c73",
    "zh:f1562cc4e74f1e838fcc9457112f515c3463ec21dfa7c3d5d6f58bbeef74ab1a",
  ]
}

provider "registry.terraform.io/hashicorp/google" {
  version     = "4.1.0"
  constraints = "~> 4.0"
  hashes = [
    "h1:oXcSMwbjSCr0PO2PyZbKPbPXnd4EhxdJGZQwtzQqwrM=",
    "zh:3477415ef4ef02d7c065a08fb24b7011ddd77f76b2a011db25e0b73c767acaae",
    "zh:43be16d0bbb56d29c090c1abe49e877e4f3ca66bb372a661a2cd0a377cd61e8f",
    "zh:6bb202a40055ed302f9dd05e2f3f87aa13080e580a9ac7b0241d634b78b68a2a",
    "zh:7179303f628c5fc58129013ef4a4e754e225e266e68af4a7167a06a64d9d97db",
    "zh:837cdcf1bafd4c936f15a4a22353efeec96ca23727f8d8ff22e9d53414d87084",
    "zh:a853f1a1e9f5ac261fd9a02d869ce7da16917fdd16464dabf2200e2ccd04fba9",
    "zh:aba631b9db47cc34857c25459475f17d939a46022f886e396a8254588f720c58",
    "zh:ac461358984ba480b4fef1d2be3538d9714aa8814a6f420e1e954227054a14a4",
    "zh:ade64689485fc28b6aa0c36861c6c9c66b5d7a8e7282739ecf97c7791b973312",
    "zh:c3fbbfe7f6e870e19bb552a2ad84ea38c9ddf3e25e0b7e996ecb2fc7ce336b03",
    "zh:d1419f4fe9c07f136ee9015a299cd6acf25a13bea0afa0f69727a576058603d3",
  ]
}

provider "registry.terraform.io/hashicorp/helm" {
  version     = "2.4.1"
  constraints = "~> 2.3"
  hashes = [
    "h1:Gqwrr+yKWR79esN39X9eRCddxMNapmaGMynLfjrUJJo=",
    "zh:07517b24ea2ce4a1d3be3b88c3efc7fb452cd97aea8fac93ca37a08a8ec06e14",
    "zh:11ef6118ed03a1b40ff66adfe21b8707ece0568dae1347ddfbcff8452c0655d5",
    "zh:1ae07e9cc6b088a6a68421642c05e2fa7d00ed03e9401e78c258cf22a239f526",
    "zh:1c5b4cd44033a0d7bf7546df930c55aa41db27b70b3bca6d145faf9b9a2da772",
    "zh:256413132110ddcb0c3ea17c7b01123ad2d5b70565848a77c5ccc22a3f32b0dd",
    "zh:4ab46fd9aadddef26604382bc9b49100586647e63ef6384e0c0c3f010ff2f66e",
    "zh:5a35d23a9f08c36fceda3cef7ce2c7dc5eca32e5f36494de695e09a5007122f0",
    "zh:8e9823a1e5b985b63fe283b755a821e5011a58112447d42fb969c7258ed57ed3",
    "zh:8f79722eba9bf77d341edf48a1fd51a52d93ec31d9cac9ba8498a3a061ea4a7f",
    "zh:b2ea782848b10a343f586ba8ee0cf4d7ff65aa2d4b144eea5bbd8f9801b54c67",
    "zh:e72d1ccf8a75d8e8456c6bb4d843fd4deb0e962ad8f167fa84cf17f12c12304e",
  ]
}

provider "registry.terraform.io/hashicorp/kubernetes" {
  version     = "2.6.1"
  constraints = "~> 2.6.0"
  hashes = [
    "h1:DWgawNO2C7IuXC2v9IjTSsqs1vZHSAbP4ilWQ0LdbwI=",
    "h1:xC0zk0wWTlWLwGLp9cBRJG8672Pp1PZQmHCA4FAJp4U=",
    "zh:081fbaf9441ebb278753dcf05f318fa7d445e9599a600d7c525e9a18b871d4c8",
    "zh:143bfbe871c628981d756ead47486e807fce876232d05607e0b8852ebee4eed8",
    "zh:34f413a644eb952e3f041d67ef19200f4c286d374eae87b60fafdd8bf6bb5654",
    "zh:370562be70233be730e1876d565710c3ef477e047f209cb3dff8a4a3217a6461",
    "zh:443021df6d56e59e4d8dda8e57b506affff32b8a22de09661d21b98bc781fefb",
    "zh:51a9501360b58adf9ee6e09fb81f555042ebc909ab36e06ccfc5e701e91f9923",
    "zh:7d41d48b8291b98e0a4b7a1f79a9d1fe140a2e0d8df422c5b48cbae4c3fa615a",
    "zh:881b3e44814d7d49a5820e2e4b13ee3d000b5baf7957df774a909f17472ece8a",
    "zh:b860ff68a944de63fbe0a624c41f2e373711a2da4298c0f0cb151e00fb32a6b3",
    "zh:c4ab48ea6e0f8d4a6db1abab1877addb2b21ecd126e505c74b8c85804bd92cbe",
    "zh:e96589575dfd31eab48fcc85466dd49895925473c60c802b346cdb4037953350",
  ]
}
